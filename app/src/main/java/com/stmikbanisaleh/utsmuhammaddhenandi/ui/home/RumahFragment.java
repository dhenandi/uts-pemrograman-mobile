package com.stmikbanisaleh.utsmuhammaddhenandi.ui.home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stmikbanisaleh.utsmuhammaddhenandi.R;


public class RumahFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState ) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_rumah, container, false);

    }
}